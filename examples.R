# Definindo diretório de trabalho
setwd('C:/Users/Usuario/stats-rlanguage')

# Ler arquivo texto
dados <- read.table('data/VAZAO_40050000.txt')
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))

# Ler arquivo csv
dados <- read.table('data/VAZAO_40050000.csv', sep = ';')
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))

# ler arquivo Excel 
library(readxl)
dados <- read_excel(('data/VAZAO_40050000.xlsx'), sheet = 1, col_names = FALSE)
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))

## Estatística Básica

# Média
mean(dados[['Vazao']])

# Mediana
median(dados[['Vazao']])

# 3.3 Percentis / Quartis
quantile(dados[['Vazao']])
quantile(dados[['Vazao']], c(.33, .66))
quantile(dados[['Vazao']], c(.10, .20, .30, .40, .50, .60, .70, .80, .90))

# Mínimo / Máximo
min(dados[['Vazao']])
max(dados[['Vazao']])

# Variância
var(dados[['Vazao']])

# Desvio Padrão
sd(dados[['Vazao']])

# Covariância
cov(dados[['Vazao']], dados[['Precip']])

# Correlação
cor(dados[['Vazao']], dados[['Precip']], c('pearson'))

# Converter diário para mensal
library(readxl)
library(zoo)

dados <- read_excel(('data/VAZAO_40050000.xlsx'), sheet = 1, col_names = FALSE)
colnames(dados) <- c('Data', 'Vazao')

z <- zoo(dados[['Vazao']], dados[['Data']])

# Média mensal
media_mensal <- aggregate(z, by=as.yearmon, FUN=mean)
media_mensal <- data.frame(media_mensal)

# Acumulado mensal
acum_mensal <- aggregate(z, by=as.yearmon, FUN=sum)
acum_mensal <- data.frame(acum_mensal)
