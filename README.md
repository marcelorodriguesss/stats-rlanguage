# R Estatística - Exemplos

### Versão: 11 setembro 2017
### Link para [download](https://gitlab.com/marcelorodriguesss/stats-rlanguage) desse material.

## Download: R e Rstudio para Windows

[http://nbcgib.uesc.br/mirrors/cran/bin/windows/base/](http://nbcgib.uesc.br/mirrors/cran/bin/windows/base/)

[www.rstudio.com/products/rstudio/download/#download](https://www.rstudio.com/products/rstudio/download/#download)

## 1. Configurações Iniciais

#### 1.1 Definir diretório de trabalho

```
setwd('C:/Users/Usuario/stats-rlanguage')
```

## 2. Leitura de arquivos

#### 2.1 Arquivo texto

```
dados <- read.table('data/VAZAO_40050000.txt')
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))
```

#### 2.2 Arquivo csv

```
dados <- read.table('data/VAZAO_40050000.csv', sep = ';')
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))
```

#### 2.3 Arquivo Excel (xlsx)

```
library(readxl)

dados <- read_excel(('data/VAZAO_40050000.xlsx'), sheet = 1, col_names = FALSE)
colnames(dados) <- c('Data', 'Vazao')
print(head(dados))
print(tail(dados))
```

## 3. Estatística Básica

#### 3.1 Média

```
mean(dados[['Vazao']])
```

#### 3.2 Mediana

```
median(dados[['Vazao']])
```

#### 3.3 Percentis / Quartis

```
quantile(dados[['Vazao']])
quantile(dados[['Vazao']], c(.33, .66))
quantile(dados[['Vazao']], c(.10, .20, .30, .40, .50, .60, .70, .80, .90))
```

#### 3.4 Mínimo / Máximo

```
min(dados[['Vazao']])
max(dados[['Vazao']])
```

#### 3.5 Variância

```
var(dados[['Vazao']])
```

#### 3.6 Desvio Padrão

```
sd(dados[['Vazao']])
```

#### 3.7 Covariância

```
cov(dados[['Vazao']], dados[['Precip']])
```

#### 3.8 Correlação

```
cor(dados[['Vazao']], dados[['Precip']], c('pearson'))
```

## 4. Distribuição de Probabilidades

### 4.1 Em Breve

## 5. Plot de dados

### 5.1 Em Breve

## 6. Comandos úteis

### 6.1 Converter diário para mensal

```
library(readxl)
library(zoo)

dados <- read_excel(('data/VAZAO_40050000.xlsx'), sheet = 1, col_names = FALSE)
colnames(dados) <- c('Data', 'Vazao')

z <- zoo(dados[['Vazao']], dados[['Data']])

# Média mensal
media_mensal <- aggregate(z, by=as.yearmon, FUN=mean)
media_mensal <- data.frame(media_mensal)

# Acumulado mensal
acum_mensal <- aggregate(z, by=as.yearmon, FUN=sum)
acum_mensal <- data.frame(acum_mensal)

```

## Referências

[www.r-tutor.com/r-introduction](http://www.r-tutor.com/r-introduction)<br/>
[http://climvis.de/read-plot-netcdf-files-r](http://climvis.de/read-plot-netcdf-files-r)<br/>
[http://geog.uoregon.edu/bartlein/courses/geog490/week04-netCDF.html](http://geog.uoregon.edu/bartlein/courses/geog490/week04-netCDF.html)<br/>